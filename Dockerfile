FROM golang:1.6.0

ADD . /go/src/gitlab.com/motorcode/message-srv

WORKDIR /go/src/gitlab.com/motorcode/message-srv
RUN go get --insecure ./...
RUN go build -o ../bin/service
CMD ["../bin/service"]