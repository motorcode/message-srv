package main

import (
	"fmt"
	"log"
	"time"

	"github.com/micro/go-micro/cmd"
	"github.com/micro/go-micro"
	"github.com/micro/message-srv/handler"
	//"github.com/micro/message-srv/message"

	//"github.com/micro/go-platform/kv"
	//"github.com/micro/go-platform/sync"
	//"github.com/micro/go-platform/sync/consul"
	_ "gitlab.contetto.io/contetto-micro/kuber-plugin"

	proto "github.com/micro/message-srv/proto/message"
)

func main() {
	cmd.Init()
	fmt.Println("Initialization of service")
	service := micro.NewService(
		micro.Name("go.micro.srv.message"),

		micro.RegisterTTL(time.Minute),
		micro.RegisterInterval(time.Second*5),
	)

	service.Init()

	/*message.Init(
		service.Server().Options().Broker,
		kv.NewKV(
			kv.Namespace("go.micro.srv.message"),
			kv.Client(service.Client()),
			kv.Server(service.Server()),
		),
		//consul.NewSync(sync.Nodes(SyncAddress)),
	)*/

	proto.RegisterMessageHandler(service.Server(), new(handler.Message))

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
